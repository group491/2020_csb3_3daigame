extends Spatial

var bl=0
var red=0
var gr=0
onready var r=$"Control/Label"
onready var g=$"Control/Label2"
onready var b=$"Control/Label3"
onready var do=$"Control3"
onready var ch=$"start"


func _on_DONE_pressed():
	if gr==30 and bl==50 and red==20:
		do.show()

func _on_TextureButton_button_up():
	ch.hide()
	


func _on_red_button_up():
	red+=10
	r.set_text("RED     - "+str(red))
	$glass1/red.visible = true	



func _on_BLUE_button_up():
	bl+=10
	b.set_text("BLUE   - "+str(bl))
	$glass1/blue.visible= true


func _on_green_button_up():
	gr+=10
	g.set_text("GREEN - "+str(gr))
	$glass1/green.visible=true


func _on_DONE2_button_up():
	get_tree().reload_current_scene()


func _on_TextureButton4_button_up():
	Scoret1.research=true
	Scoret1.vaccine=true
	var TheRoot = get_node("/root")
	var ThisScene = get_node("/root/Spatial")
	TheRoot.remove_child(ThisScene)
	ThisScene.call_deferred("free")
	var NextScene = GlobalGameData.PreviousScreen
	TheRoot.add_child(NextScene)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
