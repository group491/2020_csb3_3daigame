extends Control

onready var b=$"visual/upload"
onready var b2=$"visual/download"
onready var l=$"visual/loadingbar"
var i=1
onready var tt=$"visual"
onready var t=$"visual/stuff"

onready var e=$"visual/Button"
onready var item=$"items"

func _on_download_button_up():
	b2.disabled=true
	i=1
	while(i<12):
		
		if i==3:
			t.set_text("Calculating research data...")
		if i==5:
			t.set_text(t.get_text()+"\n"+"Analysing possible methods...")
		if i==7:
			t.set_text(t.get_text()+"\n"+"Gathering instructions...")
		if i==11:
			t.set_text(t.get_text()+"\n"+"Data successfully downloaded.")
			Scoret1.technical=true
			tt.visible=false
			item.visible=true
			e.disabled=false
			
		yield(get_tree().create_timer(1.0), "timeout")
		l.value+=10
		i+=1


func _on_upload_button_up():
	b.disabled=true
	while(i<12):
		if i==3:
			t.set_text("Connecting to server... ")
		if i==5:
			t.set_text(t.get_text()+"\n"+"Uploading observation data...")
		if i==7:
			t.set_text(t.get_text()+"\n"+"Analysing data...")
		if i==11:
			t.set_text(t.get_text()+"\n"+"Data successfully uploaded.")
		yield(get_tree().create_timer(1.0), "timeout")
		l.value+=10
		i+=1
	b2.disabled=false
	l.value=0
	


func _on_Button_button_up():
	Scoret1.technical=true
	var TheRoot = get_node("/root")
	var ThisScene = get_node("/root/tasktach")
	TheRoot.remove_child(ThisScene)
	ThisScene.call_deferred("free")
	var NextScene = GlobalGameData.PreviousScreen
	TheRoot.add_child(NextScene)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	


func _on_Button_button_down():
	item.visible=false
	tt.visible=true
