extends Spatial
onready var helmet = $"SovietHelmet"
onready var portion1 = $"Verre001"
onready var goggles = $"2869cdc1fa9d44b8a7b94b31f2003c52"
onready var portion3 = $"PotionA"
onready var portion2 = $"PotionA001"
onready var testtube = $"Cylinder001"
onready var book = $"Book_Closed_Air"
onready var gun = $"weapon"
onready var tin = $"RootNode (gltf orientation matrix)001"
onready var medicine = $"Cube010"
onready var container = $"Specimen_container"
onready var syringe = $"Cylinder"
onready var burner = $"Group001"
onready var funel = $"Cylinder003"
onready var test1 = $"Cylinder002"
onready var test2 = $"Cylinder004"
onready var test3 = $"Sphere002"
onready var test4 = $"Cylinder005"
onready var w=$"Camera/Control2"
onready var s1=$"Camera/Control"
onready var lo=$"Camera/Control3"
var count=0
var wrong=0

func _on_StaticBody_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			helmet.hide()
			wrong=1


func _on_goggles_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			goggles.hide()
			count+=1


func _on_portion3_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			portion3.hide()
			count+=1


func _on_portion2_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			portion2.hide()
			count+=1


func _on_testtube_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			testtube.hide()
			test1.hide()
			test2.hide()
			test3.hide()
			test4.hide()
			wrong=1
			
			


func _on_book_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			book.hide()
			wrong=1


func _on_gun_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			gun.hide()
			wrong=1


func _on_tin_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			tin.hide()
			wrong=1


func _on_medicine_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			medicine.hide()
			wrong=1


func _on_container_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			container.hide()
			wrong=1


func _on_syringe_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			syringe.hide()
			wrong=1


func _on_burner_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			burner.hide()
			count+=1


func _on_funel_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			funel.hide()
			count+=1


func _on_protion1_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed == true:
			portion1.hide()
			wrong=1


func _on_TextureButton_button_up():
	if count==5 and wrong==0:
		w.show()
	else:
		lo.show()
		


func _on_TextureButton2_button_up():
	Scoret1.storage=true
	var TheRoot = get_node("/root")
	var ThisScene = get_node("/root/room")
	TheRoot.remove_child(ThisScene)
	ThisScene.call_deferred("free")
	var NextScene = GlobalGameData.PreviousScreen
	TheRoot.add_child(NextScene)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _on_TextureButton3_button_up():
	var TheRoot = get_node("/root")
	var ThisScene = get_node("/root/room")
	TheRoot.remove_child(ThisScene)
	ThisScene.call_deferred("free")
	var NextScene = GlobalGameData.PreviousScreen
	TheRoot.add_child(NextScene)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
