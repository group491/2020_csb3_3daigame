extends Control




func _on_Button_button_up():
	var TheRoot = get_node("/root")
	var ThisScene = get_node("/root/done")
	TheRoot.remove_child(ThisScene)
	ThisScene.call_deferred("free")
	var NextScene = GlobalGameData.PreviousScreen
	TheRoot.add_child(NextScene)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
