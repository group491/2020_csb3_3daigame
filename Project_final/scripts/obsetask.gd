extends Interactable
func get_interaction_text():
	return "COMPLETE TASK"
func _ConfigUnit():
	
	var TheRoot = get_node("/root")  #
	var ThisScene = get_node("/root/map")
	GlobalGameData.PreviousScreen = ThisScene

	ThisScene.print_tree()
	TheRoot.remove_child(ThisScene)

	var NextScene = load("res://observation_task/Spatial.tscn")
	NextScene = NextScene.instance()
	TheRoot.add_child(NextScene)
func interact():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	_ConfigUnit()
