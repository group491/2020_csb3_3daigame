extends Spatial

class_name Weapon

export var fire_rate = 0.2
export var clip_size = 6
export var reload_rate = 1
export var default_position : Vector3
export var ads_position : Vector3
export var ads_acceleration : float = 0.3
export var default_fov : float = 70
export var ads_fov : float = 55
onready var f=$"../fire"
onready var f2=$"../reload"

onready var ammo_label = $"/root/map/UI/Label"
export var raycast_path : NodePath
export var camera_path : NodePath



var aimcast : RayCast
var camera : Camera
var current_ammo = 0
var can_fire = true
var reloading = false
var shooting = false
onready var an1=$"../../../../AnimationPlayer"
func _ready():
	current_ammo = clip_size
	aimcast = get_node(raycast_path)
	camera = get_node(camera_path)
	
func _process(delta):
	if reloading:
		ammo_label.set_text("Reloading...")
	else:
		ammo_label.set_text("%d / %d" % [current_ammo, clip_size])
	
	if Input.is_action_just_pressed("primary_fire") and can_fire:
		if current_ammo > 0 and not reloading:
			fire()
			shooting=true
		elif not reloading:
			shooting=false
			reload()
	
	if Input.is_action_just_pressed("reload") and not reloading:
		reload()
		shooting=false
	if Input.is_action_pressed("ads"):
		transform.origin = transform.origin.linear_interpolate(ads_position, ads_acceleration)
		camera.fov = lerp(camera.fov, ads_fov, ads_acceleration)
	else:
		transform.origin = transform.origin.linear_interpolate(default_position, ads_acceleration)
		camera.fov = lerp(camera.fov, default_fov, ads_acceleration)


	


func fire():
	f.play()
	print("Fired weapon")
	if aimcast.is_colliding():
		var target = aimcast.get_collider()
		if target.is_in_group("enemies"):
			print("hit enemy")
			if Scoret1.vaccine==true:
				target.health -= 50
			else:
				target.frozen=1

	can_fire = false
	current_ammo -= 1
	yield(get_tree().create_timer(fire_rate), "timeout")
	
	can_fire = true

func reload():
	f2.play()
	print("Reloading")
	reloading = true
	yield(get_tree().create_timer(reload_rate), "timeout")
	current_ammo = clip_size
	reloading = false
	print("Reload complete")
