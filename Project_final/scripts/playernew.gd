extends KinematicBody

export var speed = 10
export var acceleration = 5
export var gravity = 0.98
export var jump_power = 30
export var mouse_sensitivity = 0.2
onready var ss=$"AudioStreamPlayer"
onready var ss2=$"AudioStreamPlayer2"
onready var hb=$"../UI/ProgressBar"
onready var head = $Head
onready var camera = $Head/Camera
onready var emr=$"../UI/Label3"
onready var col=$"../UI/ColorRect"
onready var time=$"../UI/count/Timer"
onready var count=$"../UI/count"
onready var old=$"Head/Camera/weapons/pistol"
onready var new=$"Head/Camera/weapons/Iphone"
var  health=100
var velocity = Vector3()
var camera_x_rotation = 0
var sound=0
var first=0

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	

func _input(event):
	
	
	if event is InputEventMouseMotion:
		head.rotate_y(deg2rad(-event.relative.x * mouse_sensitivity))

		var x_delta = event.relative.y * mouse_sensitivity
		if camera_x_rotation + x_delta > -90 and camera_x_rotation + x_delta < 90: 
			camera.rotate_x(deg2rad(+x_delta))
			camera_x_rotation += x_delta

func _process(delta):
	
	if Input.is_action_just_pressed("ui_cancel"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _physics_process(delta):
	
	speed = 10
	sound=0
	var head_basis = head.get_global_transform().basis
	
	var direction = Vector3()

	if Input.is_action_pressed("move_forward"):
		sound=1
		direction += head_basis.z
	elif Input.is_action_pressed("move_backward"):
		direction -= head_basis.z
		sound=1
		
	if Input.is_action_pressed("move_left"):
		direction += head_basis.x
		sound=1
	
	elif Input.is_action_pressed("move_right"):
		direction -= head_basis.x
		sound=1
		
	if Input.is_action_pressed("move_forward") and Input.is_action_pressed("speed"):
		speed=18
		direction += head_basis.z
		sound=2
	elif Input.is_action_pressed("move_backward") and Input.is_action_pressed("speed"):
		speed=18
		sound=2
		direction -= head_basis.z
	
	if Input.is_action_pressed("move_left") and Input.is_action_pressed("speed"):
		speed=18
		sound=2
		direction += head_basis.x
	elif Input.is_action_pressed("move_right") and Input.is_action_pressed("speed"):
		speed=18
		sound=2
		direction -= head_basis.x
	
	direction = direction.normalized()
	
	velocity = velocity.linear_interpolate(direction * speed, acceleration * delta)
	velocity.y -= gravity
	
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y += jump_power
	if sound==1 :
		if !ss.playing:
		
			
			
			ss.play()
	elif ss.playing:
		ss.stop()
	if sound==2:
		if !ss2.playing:
			ss2.play()
	elif ss2.playing:
		ss2.stop()
	
	velocity = move_and_slide(velocity, Vector3.UP)
	if Scoret1.health<=1 and first==0 and Scoret1.vaccine==false:
		Scoret1.infected=true
		col.show()
		emr.set_text("INFECTED!! GET TO THE QUARANTINE ROOM!!")
		emr.set_visible(true)
		time.start(50)
		count.set_visible(true)
		first=1
	if Scoret1.quarantine==true:
		Scoret1.infected=false
		Scoret1.health=100
		time.stop()
		first=0
		hb.value=Scoret1.health
		count.hide()
		col.hide()
		emr.set_visible(false)
		Scoret1.quarantine=false
		
	if time.get_time_left()!=0 and time.is_stopped()==false:
		count.set_text(str(floor(time.get_time_left())))
	if Scoret1.infected==true and time.get_time_left()==0:
		count.hide()
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_tree().change_scene("res://mainscenes/lose.tscn")
	if Scoret1.research==true:
		old.hide()
		new.show()



