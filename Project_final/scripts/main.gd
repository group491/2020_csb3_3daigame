extends Spatial
var camera
var ray
var cursor_3d
onready var s2=$"Camera/Control2"
onready var s1=$"Camera/Control"
var level = 0 
func getLevel():
	var levelName = "res://quarantine_task/levels/level" + str(level) +".tscn"
	return levelName


func _ready():
	
	camera = $Camera
	ray = $Camera/RayCast
	var levelObj = load(getLevel())
	add_child(levelObj.instance())

func engine(on):
	if on:
		return 1
	else:
		return 0
		



func _physics_process(delta):
	var ray_length = 1000
	var mouse_pos = get_viewport().get_mouse_position()
	var to = camera.project_local_ray_normal(mouse_pos) * ray_length
	ray.cast_to = to
	ray.force_raycast_update()
	if ray.is_colliding():
		cursor_3d = ray.get_collision_point()
	
		
func _on_gate_body_entered(body):
	if body.is_in_group("main_cell"):
		$sounds/victory.play()
		s2.show()



func _on_TextureButton_button_up():
	s1.hide()


func _on_TextureButton2_button_up():
	Scoret1.quarantine=true
	var TheRoot = get_node("/root")
	var ThisScene = get_node("/root/main")
	TheRoot.remove_child(ThisScene)
	ThisScene.call_deferred("free")
	var NextScene = GlobalGameData.PreviousScreen
	TheRoot.add_child(NextScene)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
