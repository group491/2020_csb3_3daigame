extends KinematicBody
enum{IDLE,PURSUE,ATTACK,PATROL}
export var speed = 300
var speed2=6
var space_state
var rr=0
var path=[]
var path_node=0
var state=PATROL
var health=200
onready var player=$"../../player"
var target
var target2=null
var frozen=0
var go=0
onready var anim=$AnimationPlayer
onready var hb=$"../../UI/ProgressBar"
onready var nav=get_parent()

var way=[Vector3(40.946,0,-39.591),Vector3(-39.842,0,-38.91),Vector3(-41.478,0,40.258)]
func _ready():
	space_state = get_world().direct_space_state

func move_to(target_pos):
	path=nav.get_simple_path(global_transform.origin, target_pos)
	path_node=0
	
func _process(delta):
	
	#if Scoret1.vaccine==true:
		#move_to(player.global_transform.origin)
	if health==0:
		Scoret1.e3=true
		self.queue_free()
		
	if frozen==1:
		
		anim.play("frozendefault")
		yield(get_tree().create_timer(15),"timeout")
		frozen=0	
	
	#if target2:
		
		#look_at(target2.global_transform.origin, Vector3.UP)
		#anim.play("attackdefault")
		#Scoret1.health-=0.1
		#hb.value=Scoret1.health

		
	if target:
		state=PURSUE
		var result = space_state.intersect_ray(global_transform.origin, target.global_transform.origin)
		if result.collider.is_in_group("Player"):
			look_at(target.global_transform.origin, Vector3.UP)
			if global_transform.origin.distance_to(target.global_transform.origin)>3:
				move_to_target(delta)
				anim.play("runningdefault")
			else:
				anim.play("attackdefault")
				Scoret1.health-=0.1
				hb.value=Scoret1.health
				
		else:
			state=PATROL
	else:
			state=PATROL
	if state==PATROL:
		if path_node < path.size():
			var direction2 = (path[path_node] - global_transform.origin)
			if direction2.length() < 1:
				path_node += 1
				
			else:
				look_at(path[path_node], Vector3.UP)
				anim.play("default")
				move_and_slide(direction2.normalized() * speed * delta,Vector3.UP)
func _on_Area_body_entered(body):
	if body.is_in_group("Player"):
		target = body
		print(body.name + " entered")
		#set_color_red()
	if body.is_in_group("enemy0"):
		get_node("CollisionShape").disabled=true

func _on_Area_body_exited(body):
	if body.is_in_group("Player"):
		target = null
		print(body.name + " exited")
		anim.play("idleanim")
	if body.is_in_group("enemy0"):
		get_node("CollisionShape").disabled=false

func move_to_target(delta):
	var direction = (target.transform.origin - transform.origin).normalized()
	move_and_slide(direction * speed * delta, Vector3.UP)

func set_color_red():
	$"Skeleton/Ch16_Shirt".get_surface_material(0).set_albedo(Color(1, 0, 0))

func set_color_green():
	$"Skeleton/Ch16_Shirt".get_surface_material(0).set_albedo(Color(0, 1, 0))


func _on_way8_body_entered(body):
	if body==self:
		move_to(way[1])


func _on_way9_body_entered(body):
	if body==self:
		if go==0:
			move_to(way[2])
		elif go==1:
			move_to(way[0])


func _on_way10_body_entered(body):
	if body==self:
		if go==0:
			go=1
		else:
			go=0
	
	
	
		move_to(way[1])


func _on_Area2_body_entered(body):
	if body.is_in_group("Player"):
		
		target2=body





func _on_Area2_body_exited(body):
	if body.is_in_group("Player"):
		anim.play("idleanim")
		target2=null
