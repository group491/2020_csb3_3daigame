# 2020_CSB3_3dAiGame

To create an AI implemented 3D game.

**Description** : This game focusses on the player and an AI implemented enemy. We have 6 rooms in this game namely techinal room, observation room, research lab, storage room, cafeteria and quarantine room. Every room has a task for the player. The enemy is infected with the virus and it can infect the player with the virus if its close to it. The player has to run away from this enemy and do certain tasks in each room. On completion of these tasks the player becomes eligible for making the vaccine in the research lab. The player gets immunity to kill the enemy by aquiring the vaccine. Player wins by killing all the enemies with the vaccine as there are 4 enemies in our game. Enemy wins if the player doesnt reach the quarantine room to recover within the specified time after getting infected. 

**Methodology** : We are using the godot game engine for implementing this game. gd script language is used to code the enemy and player. Enemy is  implemented using AI. Enemy is implemented using A* pathfinding algorithm, patrol algorithm, pursue algorithm and attack algorithm. Each room then has a task assigned for the player. 

**Datasets used**: For every room and scenes we are using 3d models of objects. Some objects are imported into the game from sketchfab whereas some has been deigned by ourselves in blender. The size, texture and color of each object is then set in the godot game engine.

**Contributions : ** 

S5 CSE-B,

**Muhammad Zaruf C 25** -
1. Player script 
2. A* algorithm and path finding using navmesh for enemy
3. Map designing and layout 
4. Designing of cafeteria, quarantine room and technical room
5. Task placement in all rooms
6. Enemy movement and its kinematic and collision properties
7. Enemy animation of walking,running, attacking, freezing and dying
8. Implementation of multiple enemies
9. Shooting and gun implementation 
10. Interactables and interactions
11. Bug fixation and improving overall performance of the game.

**Nahla Mohamed 30** - 
1. Attack algorithm
2. Designing of observation room.
3. Designing of observation task.
4. Implementation of game controls.
5. Implementation of exit scene on pressing esc button.
6. Implementation of Task Win and Task Lose scenes.
7. Implementation of sound effects.
8. Implementation of raycast in first person camera for shooting. 

**Nair Amrutha Prakasan 31** - 
1. Pursue algorithm
2. Designing of research lab 
3. Implementation of research lab task
4. Implementation of quarantine task.
5. Implementation of Red Zone when infected.
6. Implementation of timer for the player to run to the quarantine room
7. Implementation of Instruction scene, Game Win and Game Over scenes
8. Implementation of Task Instructions for every task.
9. Implementation of changing scenes. 

**Sruthadev Sacheendran 56** - 
1. Patrol algorithm
2. Designing of storage room
3. Implementation of storage task.
4. Implementation of health of enemy and player.
5. Implementation of Main Menu scenes
6. Designing of splash screen
7. Implementation of player UI like life bar, texts, etc.
